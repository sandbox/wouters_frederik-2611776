<?php
Class Requestador{

  private $host;
  private $port;


/**
 * Constructor
 */
  function __construct(){
    $parts = explode(':',variable_get('requestador_socket_server', ''));
    $parts = array_filter($parts);
    if (empty($parts)) {
      drupal_set_message('Requestador server not configured.', 'error');
    }else{
      $this->port = $parts[1];
      $this->host = $parts[0];
      $this->script = variable_get('requestador_script','');
    }
  }

  function getScriptLocation(){
    return $this->script;
  }

  function getSocketConnectionString(){
    return $this->host . ':' . $this->port;
  }

  function getUrl(){
    $url = 'http://'.$this->getSocketConnectionString() . '/' ;
    return $url;
  }

  /**
   *
   */
  function post($channel, $data){
    $url = $this->getUrl() . $channel;
    $return = $this->doCurl($url, array(),'POST', false, $data);
  }

  // /**
  //  *
  //  */
  // function preparePostFields($array) {
  //   $params = array();
  //   foreach ($array as $key => $value) {
  //     $params[] = $key . '=' . urlencode($value);
  //   }
  //   return implode('&', $params);
  // }

  /**
   * Do calls to hitlijst api.
   */
  function doCurl($url, $urlParams = array(), $method = 'GET', $use_cache = true, $postfields) {
    if (isset($_GET['debug']) || $method != 'GET') {
      $use_cache = false;
    }
    $cid = preg_replace('%(\/|\.)%', '_', $url);

    if ($use_cache && ($cache = cache_get($cid, 'cache')) && !empty($cache->data)) {
      $result = $cache->data;
    }else{
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl, CURLOPT_TIMEOUT, 90);
      $proxy_ip = variable_get('proxy_server','');
      $proxy_port = variable_get('proxy_port','');
      if (!empty($proxy_port) && !empty($proxy_ip)) {
        curl_setopt($curl, CURLOPT_PROXY, $proxy_ip);
        curl_setopt($curl, CURLOPT_PROXYPORT, $proxy_port);
      }

      if ($method == 'POST') {
        $post_params = $postfields;
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_params);
      }
      else{
        $full_url = $url. '?'.http_build_query($urlParams);
        curl_setopt($curl, CURLOPT_URL, $full_url);
      }
      $data = trim(curl_exec($curl));
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $result = json_decode($data);
      cache_set($cid,  $result, 'cache', strtotime('NOW + 3 hours'));
    }
    return $result;
  }

}
