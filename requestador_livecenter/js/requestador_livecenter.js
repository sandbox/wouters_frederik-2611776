(function ($) {

    $(document).ready(function(){
      //ON SOCKET receive
      $(document).on("requestador:socket", function(event, data){
        console.log('JQUERY EVENT');
        //$.each(Drupal.settings.requestador.live_channels,function(index,item){
          // if ( item.toString() == data.channel.toString()) {
          //   console.log('EQUALS');
            Drupal.behaviors.requestador_livecenter.enrich_livestream(data.channel, data.data);
          //}
        //})
      });
   });

  Drupal.behaviors.requestador_livecenter = {
      attach:function (context, settings) {
      },
      enrich_livestream:function (channel, data){
        var json = JSON.parse(data);
        console.log('[requestador_livecenter][' + channel + '] received nid ' + json.nid);
        $("[requestador-channel="+channel+"] #node-"+json.nid).remove();
        var $html = $(json.html);
        $html.addClass('hidden');
        $("[requestador-channel="+channel+"]").prepend($html);
        $("[requestador-channel="+channel+"]").find('#node-' + json.nid).slideDown( 500, function(){
          var $this = $(this);
          $this.removeClass('hidden');
        })
      }
    };
}(jQuery));
