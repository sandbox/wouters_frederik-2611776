function dnIsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

(function ($) {

  //not in behaviors
  $(document).ready(function(){
    //ON SOCKET receive
    // request permission on page load
    document.addEventListener('DOMContentLoaded', function () {
      if (Notification.permission !== "granted")
        Notification.requestPermission();
    });

    if (typeof Drupal.settings.requestador.channels != 'undefined' && typeof io != 'undefined') {
      console.log('[IO] conecting to', Drupal.settings.requestador.remote);
      window.socket = io(Drupal.settings.requestador.remote);
      $.each(Drupal.settings.requestador.channels, function(index,item){
        console.log('[IO] subscribing to', item);
        window.socket.on(item, function(data) {
            console.log('[IO]['+item+'] socket data received:',item, data);
            //Trigger all socket event handler
            $( document ).trigger( "requestador:socket", {channel: item, data: data} );
        });
      });
    }
  });


  Drupal.behaviors.requestador = {
      attach:function (context, settings) {

          $('#desktop-notifications-config-settings-form #edit-desktop-notifications-test-submit').click(function(){
            var $channel = $('#edit-desktop-notifications-test-channel').val();
            var $title = $('#edit-desktop-notifications-test-title').val();
            var $desc = $('#edit-desktop-notifications-test-description').val();
            var $img = $('#edit-desktop-notifications-test-image-location').val();
            var $url = $('#edit-desktop-notifications-test-url').val();
            var $autoclose = $('#edit-desktop-notifications-autoclose').val();
            var $autocloseTime = $('#edit-desktop-notifications-autoclose-time').val();
            var data = {};
            data.title = $title;
            data.description = $desc;
            data.image = $img;
            data.autoClose = $autoclose;
            data.autoCloseTime = $autocloseTime;//close after x
            if ($url != '') {
              data.url = $url;
            }
            console.log('[IO] socket emit', $channel, data);
            var $input = JSON.stringify(data);
            window.socket.emit($channel, $input);
            return false;
          });

      },
      notify:function (data){
        if (!Notification) {
            alert('Desktop notifications not available in your browser. Try Chromium.');
            return;
          }

          if (Notification.permission !== "granted")
            Notification.requestPermission();
          else {
            console.log(data);
            if (dnIsJsonString(data)) {
                var parsed = JSON.parse(data)
                var icon = (typeof parsed.icon == 'undefined')? 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png' : parsed.icon;
                var options = {
                  icon: icon,
                  body: parsed.description
                };
                if (typeof parsed.sound == 'undefined'){
                  options.sound = parsed.sound;
                }
                if (typeof parsed.sticky == 'undefined'){
                  options.sticky = parsed.sticky;
                }
                if (typeof parsed.silent == 'undefined'){
                  options.silent = parsed.silent;
                }
                var notification = new Notification(parsed.title, options);
              if(typeof parsed.url != 'undefined') {
                if (parsed.autoClose) {
                  setTimeout(function() {
                    notification.close()
                  }, parsed.autoCloseTime);
                }

                notification.onclick = function () {
                  window.open(parsed.url);

                };
              }
            }else{
              var notification = new Notification('Notification', {
                icon: Drupal.settings.requestador.icon,
                body: data,
              });
            }


            // notification.onclick = function () {
            //   window.open("http://stackoverflow.com/a/13328397/1269037");
            // };

          }
      }
    };
}(jQuery));
